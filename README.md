# BeckOK

Der Beck'sche OnlineKommentar ist nur mit Account nutzbar und zeigt den Kommentar nur Paragraphen- oder gar randnummerweise an. Das wollen wir ändern: BeckOK soll einmal aus dem Link zur Titelseite eines Gesetzbuchs ein PDF mit dem gesamten Buch machen.

# Systemvorraussetzungen
`node.js` und `npm`.

Run with `node crawler.js <START_URL>`. Wird START\_URL nicht angegeben, wird die Titelseite des Grundgesetzes verwendet.

# Stand
Das Skript blättert alle aufeinanderfolgend frei zugänglichen Seiten ab der angegebenen durch, ohne sie abzuspeichern.

# Todos 
* Abspeichern, obviously
* evtl. Authentifizierung?
